// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SendLocationInterface.generated.h"
/**
 * 
 UINTERFACE()
 class UTestInterface : public UInterface
 {
 GENERATED_BODY()
 public:
 UTestInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
 };

 class ITestInterface
 {
 GENERATED_BODY()
 public:

 UFUNCTION(BlueprintNativeEvent)
 FString SomeFunction(int32 Val) const;
 };
 */

UINTERFACE()
class USendLocationInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class ISendLocationInterface
{
	GENERATED_IINTERFACE_BODY()
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SendObjectLocation(bool ShouldLook, FVector LookAtTarget);
};
