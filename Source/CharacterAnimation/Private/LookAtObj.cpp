// Fill out your copyright notice in the Description page of Project Settings.

#include "LookAtObj.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "CharacterAnimationCharacter.h"



// Sets default values
ALookAtObj::ALookAtObj()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComp"));
	StaticMeshComp->SetupAttachment(GetRootComponent());

	TriggerBoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBoxComp"));
	TriggerBoxComp->SetupAttachment(GetRootComponent());

	TriggerBoxComp->OnComponentBeginOverlap.AddDynamic(this, &ALookAtObj::OnOverlapBegin);
	TriggerBoxComp->OnComponentEndOverlap.AddDynamic(this, &ALookAtObj::OnOverlapEnd);
}

// Called when the game starts or when spawned
void ALookAtObj::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALookAtObj::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALookAtObj::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ACharacterAnimationCharacter* CharacterAnimationCharacter = Cast<ACharacterAnimationCharacter>(OtherActor);
	if (CharacterAnimationCharacter)
	{
		CharacterAnimationCharacter->SendObjectLocation(true, GetActorLocation());
	}
}

void ALookAtObj::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ACharacterAnimationCharacter* CharacterAnimationCharacter = Cast<ACharacterAnimationCharacter>(OtherActor);
	if (CharacterAnimationCharacter)
	{
		CharacterAnimationCharacter->SendObjectLocation(false, GetActorLocation());
	}
}